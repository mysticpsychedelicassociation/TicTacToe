'use strict';

const EventEmitter = require( 'events' );

class Board extends EventEmitter {
	/**
	 * @param  {String} [firstPlayer='x']
	 * @param  {String[]} initialBoard
	 */
	constructor( firstPlayer = Board.X, initialBoard ) {
		super();

		if ( initialBoard ) {
			this._array = initialBoard;
		} else {
			this.clear();
		}

		this._player = firstPlayer !== Board.O;
	}

	/**
	 * Tries to set field and change player to opponent.
	 *
	 * @emits Board#end-round
	 * @emits Board#field-state-changed
	 * @param {Number} x From 0 to 2
	 * @param {Number} y From 0 to 2
	 * @return {Boolean} false if field was used before
	 */
	set( x, y ) {
		if ( this._array[ x ][ y ] !== Board.BLANK ) {
			return false;
		}
		let player = Board.X;

		if ( !this._player ) {
			player = Board.O;
		}

		this._player = !this._player;
		this._array[ x ][ y ] = player;

		/**
		 * @event Board#field-state-changed
		 * @type {Object}
		 * @property {Number} x
		 * @property {Number} y
		 * @property {Player} player
		 */
		this.emit( 'field-state-changed', { x, y, player } );
		this._isWinner( x, y );
		return true;
	}

	/**
	 * @param  {Number} x From 0 to 2
	 * @param  {Number} y From 0 to 2
	 * @return {String} x or o
	 */
	get( x, y ) {
		return this._array[ x ][ y ];
	}

	/**
	 * Clears the board with blank spaces in fields.
	 */
	clear() {
		this._array = [
			[ ' ', ' ', ' ' ],
			[ ' ', ' ', ' ' ],
			[ ' ', ' ', ' ' ]
		];
	}

	/**
	 * @return {String}
	 */
	get player() {
		if ( this._player ) {
			return Board.X;
		}
		return Board.O;
	}

	static get X() {
		return 'x';
	}

	static get O() {
		return 'o';
	}

	static get BLANK() {
		return ' ';
	}

	/**
	 * @emits Board#end-round
	 * @param x
	 * @param y
	 * @private
	 */
	_isWinner( x, y ) {
		const lastPlayer = this._array[ x ][ y ];
		const lastMove = x * 3 + y;

		for ( const row of Board._winnerLines[ lastMove ] ) {
			const field1 = this._array[ row[ 0 ][ 0 ] ][ row[ 0 ][ 1 ] ];
			const field2 = this._array[ row[ 1 ][ 0 ] ][ row[ 1 ][ 1 ] ];

			if ( lastPlayer === field1 && lastPlayer === field2 ) {
				/**
				 * @event Board#end-round
				 * @type {Object}
				 * @property {String} winner
				 */
				this.emit( 'end-round', { winner: lastPlayer } );
				return;
			}
		}

		if ( Board._countBlank( this._array ) === 0 ) {
			/**
			 * @event Board#end-round
			 * @type {Object}
			 * @property {Null} winner
			 */
			this.emit( 'end-round', { winner: null } );
		}
	}

	/**
	 * @param board
	 * @returns {Number}
	 * @private
	 */
	static _countBlank( board ) {
		let blanks = 0;

		for ( const row of board ) {
			for ( const field of row ) {
				if ( field === Board.BLANK ) {
					++blanks;
				}
			}
		}
		return blanks;
	}

	static get _winnerLines() {
		return [
			[ [ [ 0, 1 ], [ 0, 2 ] ], [ [ 1, 0 ], [ 2, 0 ] ], [ [ 1, 1 ], [ 2, 2 ] ] ],
			[ [ [ 1, 1 ], [ 2, 1 ] ], [ [ 0, 0 ], [ 0, 2 ] ] ],
			[ [ [ 1, 1 ], [ 2, 0 ] ], [ [ 0, 0 ], [ 0, 1 ] ], [ [ 1, 2 ], [ 2, 2 ] ] ],
			[ [ [ 1, 1 ], [ 1, 2 ] ], [ [ 0, 0 ], [ 2, 0 ] ] ],
			[ [ [ 0, 1 ], [ 2, 1 ] ], [ [ 1, 0 ], [ 1, 2 ] ], [ [ 2, 0 ], [ 0, 2 ] ], [ [ 0, 0 ], [ 2, 2 ] ] ],
			[ [ [ 0, 2 ], [ 2, 2 ] ], [ [ 1, 0 ], [ 1, 1 ] ] ],
			[ [ [ 0, 0 ], [ 1, 0 ] ], [ [ 2, 1 ], [ 2, 2 ] ], [ [ 0, 2 ], [ 1, 1 ] ] ],
			[ [ [ 0, 1 ], [ 1, 1 ] ], [ [ 2, 0 ], [ 2, 2 ] ] ],
			[ [ [ 0, 0 ], [ 1, 1 ] ], [ [ 0, 2 ], [ 1, 2 ] ], [ [ 2, 0 ], [ 2, 1 ] ] ]
		];
	}
}

module.exports = Board;
