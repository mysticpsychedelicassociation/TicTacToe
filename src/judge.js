'use strict';

const Board = require( '../src/board' );
const EventEmitter = require( 'events' );

class Judge extends EventEmitter {
	/**
	 * @param {Object}
	 * @param {Number} config.pointLimit point limit of the actual game
	 * @param {String} config.firstPlayer which player starts first
	 */
	constructor( config = {} ) {
		super();

		this.pointLimit = config.pointLimit || 0;
		this.score = { x: 0, o: 0 };

		if ( config.firstPlayer === Board.O ) {
			this._isPlayerX = false;
		} else {
			this._isPlayerX = true;
		}
	}

	/**
	 * @emits Judge#end-game
	 * @emits Judge#next-round
	 * @param {Boolean} player
	 */
	pointFor( player ) {
		if ( player === Board.X ) {
			this.score.x += 1;
		}
		if ( player === Board.O ) {
			this.score.o += 1;
		}

		if ( this.pointLimit !== 0 ) {
			if ( this.pointLimit === this.score.o ) {
				/**
				 * @event Judge#end-game
				 * @type {Object}
				 * @property {String} winner
				 * @property {{x: Number, o: Number}} score
				 */
				this.emit( 'end-game', {
					winner: Board.O,
					score: this.score
				} );
				return;
			} else if ( this.pointLimit === this.score.x ) {
				this.emit( 'end-game', {
					winner: Board.X,
					score: this.score
				} );
				return;
			}
		}

		this._nextRoundEmit();
	}

	/**
	 *
	 * @private
	 */
	_nextRoundEmit() {
		if ( this._isPlayerX ) {
			/**
			 * @event Judge#next-round
			 * @type {Object}
			 * @property {String} firstPlayer
			 * @property {{x: Number, o: Number}} score
			 */
			this.emit( 'next-round', {
				firstPlayer: Board.O,
				score: this.score
			} );
		} else {
			this.emit( 'next-round', {
				firstPlayer: Board.X,
				score: this.score
			} );
		}
		this._isPlayerX = !this._isPlayerX;
	}
}

module.exports = Judge;
