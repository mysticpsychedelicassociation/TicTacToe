'use strict';

const EventEmitter = require( 'events' );

class Game extends EventEmitter {
	constructor( config ) {
		super();

		const { board, judge, player } = config;

		board.on( 'end-round', message => judge.pointFor( message.winner ) );
		judge.on( 'next-round', message => board.clear( message.firstPlayer ) );
		player.on( 'selected-field', message => board.set( message.x, message.y ) );

		this._broadcast( board, 'change-field-state' );
		this._broadcast( judge, 'next-round' );
		this._broadcast( judge, 'end-game' );
	}

	_broadcast( from, event ) {
		from.on( event, message => this.emit( event, message ) );
	}
}

module.exports = Game;
