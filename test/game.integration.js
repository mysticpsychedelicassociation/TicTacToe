'use strict';

const Game = require( '../src/game' );
const Board = require( '../src/board' );
const Judge = require( '../src/judge' );
// const Player = require( '../src/player' );
const EventEmitter = require( 'events' );

const { expect } = require( 'chai' );
const sinon = require( 'sinon' );

describe( 'Game integration test', () => {
	let nextRoundListener;
	let endGameListener;

	let board;
	let judge;
	let player;
	let game;

	beforeEach( () => {
		nextRoundListener = sinon.spy();
		endGameListener = sinon.spy();

		board = new Board();
		judge = new Judge( { pointLimit: 1 } );
		player = new EventEmitter();
		game = new Game( { board, judge, player } );

		game.on( 'next-round', nextRoundListener );
		game.on( 'end-game', endGameListener );
	} );
	it( 'O wins game in one round', () => {
		/*
		 O |   | X
		-----------
		 X | O |
		-----------
		 X |   | O
		 */
		player.emit( 'selected-field', { x: 2, y: 0 } ); // X
		player.emit( 'selected-field', { x: 0, y: 0 } ); // O

		player.emit( 'selected-field', { x: 0, y: 1 } ); // X
		player.emit( 'selected-field', { x: 1, y: 1 } ); // O

		player.emit( 'selected-field', { x: 0, y: 2 } ); // X
		player.emit( 'selected-field', { x: 2, y: 2 } ); // O

		sinon.assert.calledOnce( endGameListener );
		expect( endGameListener.args[ 0 ][ 0 ] ).to.be.deep.equal( {
			winner: Board.O,
			score: { x: 0, o: 1 }
		} );
	} );
	it( 'O wins game in one round (with bad move)', () => {
		/*
		 O |   | X
		-----------
		 X | O |
		-----------
		 X |   | O
		 */
		player.emit( 'selected-field', { x: 2, y: 0 } ); // X
		player.emit( 'selected-field', { x: 0, y: 0 } ); // O

		player.emit( 'selected-field', { x: 0, y: 1 } ); // X
		player.emit( 'selected-field', { x: 1, y: 1 } ); // O

		player.emit( 'selected-field', { x: 1, y: 1 } ); // X - bad move

		player.emit( 'selected-field', { x: 0, y: 2 } ); // X
		player.emit( 'selected-field', { x: 2, y: 2 } ); // O

		sinon.assert.calledOnce( endGameListener );
		expect( endGameListener.args[ 0 ][ 0 ] ).to.be.deep.equal( {
			winner: Board.O,
			score: { x: 0, o: 1 }
		} );
	} );
	it( 'X wins game in second round', () => {
		nextRoundListener = sinon.spy();
		endGameListener = sinon.spy();

		board = new Board();
		judge = new Judge( { pointLimit: 2 } );
		player = new EventEmitter();
		game = new Game( { board, judge, player } );

		game.on( 'next-round', nextRoundListener );
		game.on( 'end-game', endGameListener );
		/*
		.X.|   |
		-----------
		 X | O | O
		-----------
		 X |   |
		 */
		player.emit( 'selected-field', { x: 0, y: 0 } ); // X
		player.emit( 'selected-field', { x: 1, y: 1 } ); // O

		player.emit( 'selected-field', { x: 1, y: 0 } ); // X

		player.emit( 'selected-field', { x: 1, y: 2 } ); // O
		player.emit( 'selected-field', { x: 2, y: 0 } ); // X

		sinon.assert.calledOnce( nextRoundListener );
		expect( nextRoundListener.args[ 0 ][ 0 ] ).to.be.deep.equal( {
			firstPlayer: Board.O,
			score: { x: 1, o: 0 }
		} );
		/*
		   | X | O
		-----------
		   | X |
		-----------
		   | X | O
		 */
		player.emit( 'selected-field', { x: 0, y: 2 } ); // O
		player.emit( 'selected-field', { x: 0, y: 1 } ); // X

		player.emit( 'selected-field', { x: 2, y: 2 } ); // O
		player.emit( 'selected-field', { x: 2, y: 1 } ); // X

		player.emit( 'selected-field', { x: 0, y: 0 } ); // O
		player.emit( 'selected-field', { x: 1, y: 1 } ); // X

		sinon.assert.calledOnce( endGameListener );
		expect( endGameListener.args[ 0 ][ 0 ] ).to.be.deep.equal( {
			winner: Board.X,
			score: { x: 2, o: 0 }
		} );
	} );
	it( 'Expect draw in first round', () => {
		/*
		 X | O | X
		-----------
		 X | O | O
		-----------
		 O | X | X
		 */
		player.emit( 'selected-field', { x: 0, y: 0 } ); // X
		player.emit( 'selected-field', { x: 1, y: 0 } ); // O

		player.emit( 'selected-field', { x: 2, y: 0 } ); // X
		player.emit( 'selected-field', { x: 1, y: 1 } ); // O

		player.emit( 'selected-field', { x: 1, y: 2 } ); // X
		player.emit( 'selected-field', { x: 0, y: 2 } ); // O

		player.emit( 'selected-field', { x: 0, y: 1 } ); // X
		player.emit( 'selected-field', { x: 2, y: 1 } ); // O

		player.emit( 'selected-field', { x: 2, y: 2 } ); // X

		sinon.assert.calledOnce( nextRoundListener );
		expect( nextRoundListener.args[ 0 ][ 0 ] ).to.be.deep.equal( {
			firstPlayer: Board.O,
			score: { x: 0, o: 0 }
		} );
	} );
} );
