'use strict';

const Board = require( '../src/board' );
const sinon = require( 'sinon' );
const { expect } = require( 'chai' );

describe( 'Board', () => {
	describe( 'consts', () => {
		it( 'X should be x', () => {
			expect( Board.X ).to.be.equal( 'x' );
		} );
		it( 'O should be o', () => {
			expect( Board.O ).to.be.equal( 'o' );
		} );
		it( 'Blank should be space', () => {
			expect( Board.BLANK ).to.be.equal( ' ' );
		} );
	} );
	describe( 'constructor()', () => {
		it( 'default player should be X', () => {
			const board = new Board();

			expect( board.player ).to.be.equal( Board.X );
		} );
		it( 'should set first player', () => {
			const board = new Board( Board.O );

			expect( board.player ).to.be.equal( Board.O );
		} );
		it( 'should set initial state', () => {
			const X = Board.X;
			const O = Board.O;
			const B = Board.BLANK;
			const board = new Board( O, [
				[ B, B, B ],
				[ B, X, B ],
				[ B, B, B ]
			] );

			expect( board.get( 1, 1 ) ).to.be.equal( Board.X );
		} );
	} );
	describe( 'set()', () => {
		describe( 'first move', () => {
			it( 'should set X on choosen field', () => {
				const board = new Board();

				board.set( 1, 1 );

				expect( board.get( 1, 1 ) ).to.be.equal( Board.X );
			} );
			it( 'should set O on choosen field', () => {
				const board = new Board( Board.O );

				board.set( 1, 1 );

				expect( board.get( 1, 1 ) ).to.be.equal( Board.O );
			} );
			it( 'should change player X to O', () => {
				const board = new Board();

				board.set( 1, 1 );

				expect( board.player ).to.be.equal( Board.O );
			} );
			it( 'should change player O to X', () => {
				const board = new Board( Board.O );

				board.set( 1, 1 );

				expect( board.player ).to.be.equal( Board.X );
			} );
			it( 'should return true if field before set was blank', () => {
				const board = new Board();

				expect( board.set( 1, 1 ) ).to.be.true;
			} );
			it( 'should emit field-state-changed event', () => {
				const board = new Board();
				const listener = sinon.spy();
				board.on( 'field-state-changed', listener );

				board.set( 1, 1 );

				sinon.assert.calledOnce( listener );
				const [data] = listener.getCall( 0 ).args;
				expect( data ).to.be.deep.equal( {
					x: 1,
					y: 1,
					player: Board.X
				} );
			} );
		} );
		describe( 'second move', () => {
			it( 'should set O on choosen field', () => {
				const board = new Board();
				board.set( 0, 0 );

				board.set( 1, 1 );

				expect( board.get( 1, 1 ) ).to.be.equal( Board.O );
			} );
			it( 'should set X on choosen field', () => {
				const board = new Board( Board.O );
				board.set( 0, 0 );

				board.set( 1, 1 );

				expect( board.get( 1, 1 ) ).to.be.equal( Board.X );
			} );
			it( 'should not change player if field was occupied', () => {
				const board = new Board();
				board.set( 0, 0 );

				board.set( 0, 0 );

				expect( board.player ).to.be.equal( Board.O );
			} );
			it( 'should return false if field was occupied', () => {
				const board = new Board();
				
				board.set( 0, 0 );

				expect( board.set( 0, 0 ) ).to.be.false;
			} );
			it( 'should not emit change-field-state event if field was occupied', () => {
				const board = new Board();
				const listener = sinon.spy();
				board.on( 'change-field-state', listener );

				board.set( 1, 1 );

				sinon.assert.notCalled( listener );
			} );
		} );
		describe( 'winner conditions', () => {
			it( 'should emit end-round after board is full', () => {
				const X = Board.X;
				const O = Board.O;
				const B = Board.BLANK;
				const board = new Board( X, [
					[ X, O, X ],
					[ O, X, O ],
					[ O, B, O ]
				] );
				const listener = sinon.stub();
				board.on( 'end-round', listener );

				board.set( 2, 1 );

				sinon.assert.calledOnce( listener );
			} );
			it( 'should emit end-round with X as a winner property', () => {
				const X = Board.X;
				const O = Board.O;
				const B = Board.BLANK;
				const board = new Board( X, [
					[ X, X, B ],
					[ O, X, O ],
					[ O, B, B ]
				] );
				const listener = sinon.stub();

				board.on( 'end-round', listener );

				board.set(0, 2);

				sinon.assert.calledOnce( listener );
				const [ data ] = listener.getCall( 0 ).args;
				expect( data ).to.have.property( 'winner', Board.X );
			} );
			it( 'should emit end-round with O as a winner property', () => {
				const X = Board.X;
				const O = Board.O;
				const B = Board.BLANK;
				const board = new Board( O, [
					[ X, X, B ],
					[ O, B, O ],
					[ O, B, B ]
				] );
				const listener = sinon.stub();

				board.on( 'end-round', listener );

				board.set(1, 1);

				sinon.assert.calledOnce( listener );
				const [ data ] = listener.getCall( 0 ).args;
				expect( data ).to.have.property( 'winner', Board.O );
			} );
			it( 'should emit end-round with null after board is full and winner was not selected', () => {
				const X = Board.X;
				const O = Board.O;
				const B = Board.BLANK;
				const board = new Board( X, [
					[ O, X, X ],
					[ X, X, O ],
					[ O, O, B ]
				] );
				const listener = sinon.stub();

				board.on( 'end-round', listener );

				board.set( 2, 2 );

				sinon.assert.calledOnce( listener );
				const [ data ] = listener.getCall( 0 ).args;
				expect( data ).to.have.property( 'winner', null );
			} );
		} );
	} );
} );
