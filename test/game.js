'use strict';

const EventEmitter = require( 'events' );
const Game = require( '../src/game' );
const Board = require( '../src/board' );
const sinon = require( 'sinon' );

describe( 'Game', () => {
	let game;
	let board;
	let player;
	let judge;

	beforeEach( () => {
		board = new EventEmitter();
		board.set = sinon.spy();
		board.clear = sinon.spy();

		judge = new EventEmitter();
		judge.pointFor = sinon.spy();

		player = new EventEmitter();

		game = new Game( { board, judge, player } );
	} );
	describe( 'Board events - ', () => {
		describe( 'on end-round ', () => {
			it( 'should add a point', () => {
				board.emit( 'end-round', { winner: Board.X } );

				sinon.assert.calledOnce( judge.pointFor );
				sinon.assert.calledWithExactly( judge.pointFor, Board.X );
			} );
		} );
	} );
	describe( 'Judge events - ', () => {
		describe( 'on next-round', () => {
			it( 'should clear board', () => {
				judge.emit( 'next-round', { firstPlayer: Board.O } );

				sinon.assert.calledOnce( board.clear );
				sinon.assert.calledWithExactly( board.clear, Board.O );
			} );
			it( 'should emit next-round', () => {
				const listener = sinon.spy();
				game.on( 'next-round', listener );

				judge.emit( 'next-round', { firstPlayer: Board.O } );

				sinon.assert.calledOnce( listener );
			} );
		} );
		describe( 'on end-game', () => {
			it( 'should not clear board', () => {
				judge.emit( 'end-game', { winner: Board.O } );

				sinon.assert.notCalled( board.clear );
			} );
			it( 'should emit end-game', () => {
				const listener = sinon.spy();
				game.on( 'end-game', listener );

				judge.emit( 'end-game', { winner: Board.O } );

				sinon.assert.calledOnce( listener );
			} );
		} );
	} );
	describe( 'Player events - ', () => {
		describe( 'on selected-field', () => {
			it( 'selected-field', () => {
				player.emit( 'selected-field', { x: 1, y: 2 } );

				sinon.assert.calledOnce( board.set );
				sinon.assert.calledWithExactly( board.set, 1, 2 );
			} );
		} );
	} );
} );
