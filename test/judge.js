'use strict';

const Judge = require( '../src/judge' );
const Board = require( '../src/board' );
const sinon = require( 'sinon' );
const { expect } = require( 'chai' );

describe( 'Judge', () => {
	describe( 'pointFor()', () => {
		it( 'should add point for X player', () => {
			const judge = new Judge();

			judge.pointFor( Board.X );

			expect( judge.score ).to.be.deep.equal( { x: 1, o: 0 } );
		} );
		it( 'should add point for O player', () => {
			const judge = new Judge();

			judge.pointFor( Board.O );

			expect( judge.score ).to.be.deep.equal( { x: 0, o: 1 } );
		} );
		it( 'should emit next-round and change first player to O', () => {
			const judge = new Judge();
			const listener = sinon.stub();
			judge.on( 'next-round', listener );

			judge.pointFor( Board.X );

			sinon.assert.calledOnce( listener );
			const [ data ] = listener.args[ 0 ];
			expect( data ).to.have.property( 'firstPlayer', Board.O );
		} );
		it( 'should emit next-round and change first player to X', () => {
			const judge = new Judge( { firstPlayer: Board.O } );
			const listener = sinon.stub();
			judge.on( 'next-round', listener );

			judge.pointFor( Board.X );

			sinon.assert.calledOnce( listener );
			const [ data ] = listener.args[ 0 ];
			expect( data ).to.have.property( 'firstPlayer', Board.X );
		} );
		it( 'should emit next-round with score', () => {
			const judge = new Judge( { firstPlayer: Board.O } );
			const listener = sinon.stub();
			judge.on( 'next-round', listener );

			judge.pointFor( Board.X );

			sinon.assert.calledOnce( listener );
			const [ data ] = listener.args[ 0 ];
			expect( data ).to.have.property( 'score', judge.score );
		} );
		it( 'should emit end-game if player X wins', () => {
			const judge = new Judge( { pointLimit: 1 } );
			const listener = sinon.stub();
			judge.on( 'end-game', listener );

			judge.pointFor( Board.X );

			sinon.assert.calledOnce( listener );
			const [ data ] = listener.args[ 0 ];
			expect( data ).to.be.deep.equal( { winner: Board.X, score: { x: 1, o: 0 } } );
		} );
		it( 'should emit end-game if player O wins', () => {
			const judge = new Judge( { pointLimit: 1 } );
			const listener = sinon.stub();
			judge.on( 'end-game', listener );

			judge.pointFor( Board.O );

			sinon.assert.calledOnce( listener );
			const [ data ] = listener.args[ 0 ];
			expect( data ).to.be.deep.equal( { winner: Board.O, score: { x: 0, o: 1 } } );
		} );
		it( 'should not emit end-game if points limit is not provide', () => {
			const judge = new Judge();
			const listener = sinon.stub();
			judge.on( 'end-game', listener );

			judge.pointFor( Board.O );

			sinon.assert.notCalled( listener );
		} );
		it( 'should change player to opposite after next-round emit', () => {
			const judge = new Judge();
			const listener = sinon.stub();
			judge.on( 'next-round', listener );
			judge.pointFor( Board.X );

			judge.pointFor( Board.O );

			sinon.assert.calledTwice( listener );
			const [ data ] = listener.args[ 1 ];
			expect( data ).to.have.property( 'firstPlayer', Board.X );
		} );
		it( 'should emit next-round if the pointLimit was not achieved', () => {
			const judge = new Judge( { pointLimit: 2 } );
			const nextRoundListener = sinon.stub();
			const endGameListener = sinon.stub();

			judge.on( 'next-round', nextRoundListener );
			judge.on( 'end-game', endGameListener );

			judge.pointFor( Board.X );

			sinon.assert.notCalled( endGameListener );
			sinon.assert.calledOnce( nextRoundListener );
		} );
	} );
} );
